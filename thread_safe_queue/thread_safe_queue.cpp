#include "thread_safe_queue.h"
#include <thread>
#include <cstdlib>
#include <iostream>
#include <chrono>


//������: �������� �� ����� ��������
//producer ���������� ��������� �����, consumer ������ ������, ������� ������ ���� �����

int main() {
	thread_safe_queue<int> q(3);
	std::mutex printing_mutex;
	std::thread producer([&q]() {
		while (q.is_available())
			q.enqueue(rand() % 100);
	});
	producer.detach();
	std::atomic<int> numberOfConsumers;
	for (int i = 0; i < 4; ++i) {
		std::thread consumer([&q, &printing_mutex, &numberOfConsumers]() {
			while (q.is_available()) {
				bool isPrime = true;
				int number;
				q.pop(number);
				int bound = (int)sqrt(number);
				for (int divider = 2; divider <= bound && isPrime; ++divider) {
					if (number % divider == 0)
						isPrime = false;
				}
				std::lock_guard<std::mutex> lck(printing_mutex);
				std::cout << number << (isPrime ? " is prime" : " is not prime") << std::endl;
			}
			numberOfConsumers.fetch_sub(1);
		});
		numberOfConsumers.fetch_add(1);
		consumer.detach();
	}
	std::this_thread::sleep_for(std::chrono::milliseconds(100));
	q.shutdown();
	while (numberOfConsumers.load() > 0) {}
	return 0;
}