#include "SegmentTree.h"
#include <iostream>

int main() {
	int numberOfElements;
	std::cin >> numberOfElements;
	std::vector<long long int> elements;
	elements.reserve(numberOfElements);
	int element;
	for (int i = 0; i < numberOfElements; ++i) {
		std::cin >> element;
		elements.push_back(element);
	}
	SegmentTree<long long int, std::plus<long long int>, 0> tree(elements);
	int numberOfQueries;
	std::cin >> numberOfQueries;
	std::vector<long long int> answers;
	char command;
	int first, second;
	for (int i = 0; i < numberOfQueries; ++i) {
		std::cin >> command >> first >> second;
		if (command == 's')
			answers.push_back(tree.findResult(first - 1, second - 1));
		else
			tree.update(first - 1, second);
	}
	for (std::vector<long long int>::iterator it = answers.begin(); it != answers.end(); ++it)
		std::cout << *it << ' ';
	std::cout << std::endl;
	return 0;
}