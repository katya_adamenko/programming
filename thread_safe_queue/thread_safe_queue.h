#ifndef THREAD_SAFE_QUEUE
#define THREAD_SAFE_QUEUE

#include <queue>
#include <mutex>
#include <condition_variable>
#include <atomic>

template <class T>
class thread_safe_queue {
public:
	thread_safe_queue(std::size_t capacity) : capacity(capacity), available(true) {}
	thread_safe_queue(const thread_safe_queue &) = delete;
	void enqueue(T item);
	void pop(T & item);
	bool is_available()const {
		return available.load();
	}
	void shutdown() {
		available.store(false);
	}
private:
	std::queue<T> items;
	const std::size_t capacity;
	std::mutex queue_mutex;
	std::condition_variable not_empty_cv;
	std::condition_variable not_full_cv;
	std::atomic<bool> available;
};

template <class T>
void thread_safe_queue<T>::enqueue(T item) {
	std::unique_lock<std::mutex> lck2(queue_mutex);
	if (items.size() == capacity) {
		not_full_cv.wait(lck2, [this]() {return items.size() != capacity;});
	}
	items.push(item);
	not_empty_cv.notify_one();
}

template <class T>
void thread_safe_queue<T>::pop(T & item) {
	std::unique_lock<std::mutex> lck1(queue_mutex);
	if (items.empty()) {
		not_empty_cv.wait(lck1, [this]() {return !items.empty(); });
	}
	item = items.front();
	items.pop();
	not_full_cv.notify_one();
}

#endif