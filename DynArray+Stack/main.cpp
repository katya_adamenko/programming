#include "DynArray.h"

int main() { //�������� ��������� ������
	DynArray<int>array(1);
	for (unsigned int i = 0; i < 5; ++i)
		array.push_back(i + 1);
	for (unsigned int i = 0; i < array.getSize(); ++i)
		cout << array[i] << ' ';
	try {
		array.erase(5);
	}
	catch (out_of_range & ex) {
		cerr << ex.what() << endl;
	}
	try {
		array.erase(0);
	}
	catch (out_of_range & ex) {
		cerr << ex.what() << endl;
	}
	for (unsigned int i = 0; i < array.getSize(); ++i)
		cout << array[i] << ' ';
	cout << endl;
	try {
		array.pop_back();
	}
	catch (out_of_range & ex) {
		cerr << ex.what() << endl;
	}
	for (unsigned int i = 0; i < array.getSize(); ++i)
		cout << array[i] << ' ';
	cout << endl;
	try {
		array.insert(7, 2);
	}
	catch (out_of_range & ex) {
		cerr << ex.what() << endl;
	}
	for (unsigned int i = 0; i < array.getSize(); ++i)
		cout << array[i] << ' ';
	cout << endl;
	try {
		array.insert(10, 5);
	}
	catch (out_of_range & ex) {
		cerr << ex.what() << endl;
	}
	for (unsigned int i = 0; i < array.getSize(); ++i)
		cout << array[i] << ' ';
	cout << endl;
	try {
		array.push_back(10);
	}
	catch (out_of_range & ex) {
		cerr << ex.what() << endl;
	}
	for (unsigned int i = 0; i < array.getSize(); ++i)
		cout << array[i] << ' ';
	cout << endl;
	try {
		array.insert(1, 1);
	}
	catch (out_of_range & ex) {
		cerr << ex.what() << endl;
	}
	cout << endl;

	cout << "iterator: " << endl;// �������� ���������
	for (DynArray<int>::iterator it = array.begin(); it != array.end(); ++it)
		cout << *it << ' ';
	cout << endl;
	cout << "end: "  << *(array.end() - 1) << endl;
	cout << "begin: " << *(array.begin()) << endl;
	cout << "size: " << array.getSize() << "; array.end() - array.begin() " << array.end() - array.begin() << endl;
	DynArray<int>::iterator it = array.begin();
	cout << *(it + 3) << endl;
	it += 1;
	cout << *it << endl;
	cout << *(++it) << ' ';
	cout << *(it++) << endl;
	cout << *it << endl;
	cout << *(it - 1) << endl;
	--it;
	cout << *it << ' ';
	cout << *(it--) << endl;
	cout << *it << endl;
	it -= 1;
	cout << *it << endl;
	if (it == array.begin())
		cout << "yes!" << endl;
	else
		cout << "no!" << endl;
	if (it != array.end())
		cout << "yes!" << endl;
	else
		cout << "no!" << endl;
	if (it <= array.end())
		cout << "yes!" << endl;
	else
		cout << "no!" << endl;
	if (it > array.end())
		cout << "no!" << endl;
	else
		cout << "yes!" << endl;
	for (DynArray<int>::iterator it1 = array.begin(); it1 < array.end(); ++it1)
		cout << *it1 << ' ';
	cout << endl;
	for (DynArray<int>::iterator it1 = array.begin(); it1 < array.end(); ++it1)
		*it1 += 5;
	for (DynArray<int>::iterator it1 = array.end() - 1; it1 >= array.begin(); --it1)
		cout << *it1 << ' ';
	cout << endl;
	cout << endl;

	DynArray<char *> d;
	d.push_back("first ");
	d.push_back("second");
	d.push_back("end");
	for (unsigned int i = 0; i < d.getSize(); ++i)
		cout << d[i] << endl;
	try {
		d.insert("third", 1);
	}
	catch (out_of_range& ex) {
		cout << ex.what() << endl;
	}
	for (unsigned int i = 0; i < d.getSize(); ++i)
		cout << d[i] << endl;
	try {
		d.erase(2);
	}
	catch (out_of_range& ex) {
		cout << ex.what() << endl;
	}
	for (unsigned int i = 0; i < d.getSize(); ++i)
		cout << d[i] << endl;
	try {
		while (d.getSize() >= 0)
			d.pop_back();
	}
	catch (out_of_range& ex) {
		cout << ex.what() << endl;
	}
	char c;
	cin >> c;
	return 0;
}